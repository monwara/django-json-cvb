from __future__ import absolute_import, unicode_literals

import json

from django.http import HttpResponse
from django.db.models import Model
from django.db.models.query import QuerySet
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.views.generic import DetailView, ListView, TemplateResponseView


class JSONEncoder(DjangoJSONEncoder):
    """ Expands DjangoJsonEncoder to handle Django objects

    In order for related objects to appear in the JSON dump, you need to select
    them with ``select_related()`` call on the query set. Otherwise, their
    appearance depends on whether you have tried to access them at least once,
    which is not reliable.

    """

    def _obj_to_dict(self, o):
        """ Convert a Django object to plain dict

                   !!! HACK ALERT !!!

        """

        # Get objects's dict. This gives us most fields, but some fields
        # that reference other objects must be processed further later on.
        data = o.__dict__

        # Delete the Modelstate
        del data['_state']

        keys = data.keys()

        # Look for any keys that end with _cache and also have a counterpart
        # that end with _id, without the leading underscore.
        for key in keys:
            if key.endswith('_cache'):
                base_key = key.replace('_cache', '')[1:]

                # Do we have a match?
                if '%s_id' % base_key in data:
                    # Serialize the related object as well
                    data[base_key] = self._obj_to_dict(data[key])
                    # Remember the keys to remove later
                    del data[key]
                    del data['%s_id' % base_key]

        # Serialize object as usual and return
        return data


    def default(self, o):
        if isinstance(o, Model):
            return self._obj_to_dict(o)

        elif isinstance(o, QuerySet):
            return [self._obj_to_dict(obj) for obj in o]

        else:
            return super(JSONEncoder, self).default(o)


class JSONResponseMixin(object):
    """ Based on JSONResponseMixin from Django documentation

    http://bit.ly/14WZWka

    """

    response_class = HttpResponse

    def get_response_class(self):
        return self.response_class

    def get_response_kwargs(self, kwargs):
        kwargs['content_type'] = 'application/json'
        return kwargs

    def render_to_json_response(self, context, **kwargs):
        response_kwargs = self.get_response_kwargs(kwargs)
        response_class = self.get_response_class()
        return response_class(self.convert_context_to_json(context),
                              **response_kwargs)

    def render_to_response(self, context, **kwargs):
        return self.render_to_json_response(context, **kwargs)

    def convert_context_to_json(self, context, **kwargs):
        return json.dumps(context, cls=JSONEncoder, ensure_ascii=False)


class JSONView(JSONResponseMixin, TemplateView):
    pass


class JSONDetailView(JSONResponseMixin, DetailView):
    pass


class JSONListView(JSONResponseMixin, ListView):
    pass

